	.extern 	TIME
	.extern 	HEX_DISPLAY
	.extern 	COUNTER
	.extern 	STOPWATCH_STATUS
	.global 	TIMER_ISR
	
TIMER_ISR:
	/* store used registers */
	subi 	sp, sp, 28
	stw		r2, (sp)
	stw		r3, 4(sp)
	stw		r4, 8(sp)
	stw		r5, 12(sp)
	stw		r6, 16(sp)
	stw		r7, 20(sp)
	stw		r8, 24(sp)
	
	movia 	r2, TIME
	movia	r3, 0x10002000					/* TIMER base address */
	stwio 	r0, (r3)						/* write to clear interrupt bit */
	
	ldw		r4, 0(r2)						/* MINUTES */
	ldw		r5, 4(r2)						/* SECOND1 */
	ldw		r6, 8(r2)						/* SECOND0 */
	ldw		r7, 12(r2)						/* TENTH-SECOND */
	
	/* check if it's stop */
	ldw		r8, STOPWATCH_STATUS(r0)
	beq		r8, r0, END_TIMER_ISR			/* if it's in stop state (0), skip to END_TIMER, don't increment */
	
	/* increment tenth-second, but got to check if it's 9 */
	movi	r8, 9
	bne		r7, r8, NO_INC_SECOND0
	
	INC_SECOND0:
		mov		r7, r0
		stw		r7, 12(r2)
		/* increment second0, but got to check if it's 9 */
		movi	r8, 9
		bne 	r6, r8, NO_INC_SECOND1
		
		INC_SECOND1:
			mov 	r6, r0
			stw		r6, 8(r2)
			/*increment second1, but got to check if it's 5 */
			movi 	r8, 5
			bne 	r5, r8, NO_INC_MINUTES
			
			INC_MINUTES:
				mov		r5, r0
				stw		r5, 4(r2)
				/* increment minutes, but got to check if it's 9 */
				movi	r8, 9
				bne		r4, r8, NO_INC_MAX
				
				/*this is the max, nowhere to go already.. :(, so just hang at 9.59.9 and wait.. */
				movi	r4, 9
				stw		r4, (r2)
				movi	r5, 5
				stw		r5, 4(r2)
				movi	r6, 9
				stw		r6, 8(r2)
				movi	r7, 9
				stw		r7, 12(r2)
				br 		END_TIMER_ISR
				
				NO_INC_MAX:
				addi 	r4, r4, 1
				stw 	r4, (r2)	
				br 		END_TIMER_ISR
			
			NO_INC_MINUTES:
			addi 	r5, r5, 1
			stw 	r5, 4(r2)		
			br 		END_TIMER_ISR
		
		NO_INC_SECOND1:
		addi	r6, r6, 1
		stw		r6, 8(r2)
		br 		END_TIMER_ISR
	
	NO_INC_SECOND0:
	addi	r7, r7, 1
	stw		r7, 12(r2)
	br 		END_TIMER_ISR	
			
END_TIMER_ISR:
	/* restore from stack */	
	ldw		r2, (sp)
	ldw		r3, 4(sp)
	ldw		r4, 8(sp)
	ldw		r5, 12(sp)
	ldw		r6, 16(sp)
	ldw		r7, 20(sp)
	ldw		r8, 24(sp)
	addi 	sp, sp, 28			
	
	ret
	
	.end