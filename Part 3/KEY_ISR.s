	.extern 	TIME
	.extern 	HEX_DISPLAY
	.extern 	COUNTER
	.extern 	STOPWATCH_STATUS
	.global 	KEY_ISR
	
KEY_ISR:
	/* store used registers */
	subi sp, sp, 24
	stw		r2, (sp)
	stw		r3, 4(sp)
	stw		r4, 8(sp)
	stw		r5, 12(sp)
	stw 	r6, 16(sp)
	stw 	r7, 20(sp)
	
	movia 	r2, STOPWATCH_STATUS
	movia	r3, TIME
	
	movia 	r4, 0x10000050					/* KEY base address */
	ldwio 	r5, 12(r4)						/* read from edgecapture register of KEY that is 12 words higher */
	stwio 	r0, 12(r4)						/* write to clear interrupt bit */
	
	andi 	r6, r5, 0b1000					/* mask to read for KEY3 only */
	bne 	r6, r0, CHECK_KEY3
	andi 	r6, r5, 0b0100					/* mask to read for KEY2 only */
	bne 	r6, r0, CHECK_KEY2
	andi 	r6, r5, 0b0010					/* mask to read for KEY1 only */
	bne 	r6, r0, CHECK_KEY1
	
	br 		END_KEY_ISR						/* else if no condition is met, branch to the end */

CHECK_KEY3:									/* reset button */
	stw		r0, 0(r3)						/* reset everything */
	stw 	r0, 4(r3)
	stw		r0, 8(r3)
	stw 	r0, 12(r3)
	movi	r17, 3 /*debug*/
	br END_KEY_ISR
	
CHECK_KEY2:									/* stop button */
	stw		r0, (r2)						/* store 0 into stopwatch status memory */
	movi	r17, 2 /*debug*/
	br END_KEY_ISR

CHECK_KEY1:
	movi 	r7, 1
	stw 	r7, (r2)						/* write 1 into stopwatch status memory */
	movi	r17, 1 /*debug*/
	br END_KEY_ISR	

		
END_KEY_ISR:
	/* restore from stack */
	ldw		r2, (sp)
	ldw		r3, 4(sp)
	ldw		r4, 8(sp)
	ldw		r5, 12(sp)
	ldw 	r6, 16(sp)
	ldw 	r7, 20(sp)
	addi 	sp, sp, 24
	ret
	
	.end
	