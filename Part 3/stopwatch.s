	.text
	.global		_start

_start:	
	movia 	sp, 0x4500
	movia 	r2, 0x10000020				/* for HEX0-3 display */
	movia 	r3, 0x10002000				/* status register */
	movia 	r14, 0x10000050				/* KEY base address */	
	
	/* set up delay */
	ldw 	r4, COUNTER(r0)				/* load counter for timer */
	sthio 	r4, 8(r3)					/* store half word in low counter */
	srli 	r4, r4, 16					/* logical shift right by 16 */
	sthio 	r4, 12(r3)					/* store half word in high counter */
	movi 	r4, 0b0111					/* control register: set START=1 and CONT=1, also write into control register 4 words above status register at bit 1 for ITO enable */
	sthio 	r4, 4(r3)					/* write into control register */
	
	/* set up interrupt in KEY */
	movi 	r5, 0b01110					/* check interrupt in KEY 3,2,1 only */
	stwio 	r5, 8(r14)					/* write into interruptmask register which is 8 words above base address */
	movi 	r5, 0b011					/* IRQ0 for timer and IRQ1 for KEY interrupt assignment */
	wrctl 	ienable, r5
	movi 	r5, 1
	wrctl 	status, r5					/* enable PIE in status register */
	
	DISPLAY:
		
		movia	r6, TIME
		
		ldw 	r10, (r6)				/* MINUTES: recall from memory */
		call 	CONVERT_DEC_TO_HEX
		mov 	r7, r11					/* move to temp register r7 */
		slli 	r7, r7, 24				/* shift by 24 for HEX3 */
		
		ldw 	r10, 4(r6)				/* SECOND1: recall from memory */
		call 	CONVERT_DEC_TO_HEX
		mov 	r8, r11					/* move to temp register r8 */
		slli 	r8, r8, 16				/* shift by 16 for HEX2 */
		
		ldw 	r10, 8(r6)				/* SECOND0: recall from memory */
		call 	CONVERT_DEC_TO_HEX
		mov 	r9, r11					/* move to temp register r9 */
		slli 	r9, r9, 8				/* shift by 8 for HEX1 */
		
		ldw 	r10, 12(r6)				/* TENTH-SECOND: recall from memory */
		call 	CONVERT_DEC_TO_HEX
		mov 	r12, r11				/* move to temp register r9 */
		
		/* combining registers to form a complete HEX0-3 display */
		or		r13, r7, r8
		or 		r13, r13, r9
		or 		r13, r13, r12
		
		stwio	r13, (r2)				/* form pattern on HEX3-0 display */	
	
	br 		DISPLAY
	
	
	CONVERT_DEC_TO_HEX:					/* input r10, output r11 */
		subi 	sp, sp, 4
		stw 	r2, (sp)
		/* slli 	r10, r10, 2			multiplier, each time needs to be incremented by 4 (shift left by 2 bit) */
		movia 	r2, HEX_DISPLAY			/* get address of HEX_DISPLAY data */
		add 	r2, r2, r10				/* add in the multiplier */
		ldw		r11, (r2)				/* load from memory */		
		ldw		r2, (sp)
		addi 	sp, sp, 4
	ret

	.data
.global		TIME
	TIME:
		.word 	0,0,0,0					/* minutes, second1, second0, tenth-second */
.global 	HEX_DISPLAY
	HEX_DISPLAY:
		.byte 	0b0111111				/* for 0 */
		.byte 	0b0000110				/* for 1 */
		.byte 	0b1011011				/* for 2 */
		.byte	0b1001111				/* for 3 */
		.byte 	0b1100110				/* for 4 */
		.byte 	0b1101101				/* for 5 */
		.byte 	0b1111101				/* for 6 */
		.byte 	0b0100111				/* for 7 */
		.byte 	0b1111111				/* for 8 */
		.byte 	0b1101111				/* for 9 */
.global 	COUNTER
	COUNTER:
		.word 	5000000					/* to count at every tenth of a second */
.global		STOPWATCH_STATUS
	STOPWATCH_STATUS:
		.word	0						/* 0 indicates stop, 1 indicates start */