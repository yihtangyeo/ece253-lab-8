	.extern PATTERN_BACK_END
	.extern PATTERN_FRONT_END
	.extern DELAY_VALUE
	.extern LED_DIRECTION
	.extern LED_PATTERN
	.extern LED_RUNNING
	.global TIMER_ISR
	
TIMER_ISR:
	/* store used registers */
	subi sp, sp, 32
	stw r10, 0(sp)
	stw r15, 4(sp)
	stw r5, 8(sp)
	stw r2, 12(sp)
	stw r3, 16(sp)
	stw r4, 20(sp)
	stw r13, 24(sp)
	stw r14, 28(sp)
	
	movia r13, LED_DIRECTION
	movia r14, LED_PATTERN
	
	movia r10, 0x10002000					/* TIMER base address */
	stwio r0, (r10)							/* write to clear interrupt bit */
	
	ldw r15, (r14)							/* read LED_PATTERN */
	
	ldw r5, LED_RUNNING(r0)					/* read if LED is currently running */
	beq r5, r0, END_TIMER_ISR				/* if LED_RUNNING is 0 (not running), go right to END_TIMER_ISR */
	
	ldw r2, (r13)							/* read LED DIRECTION */
	beq r2, r0, SHIFT_RIGHT					/* if r2 = 0, indicates shift right */
		
	SHIFT_LEFT:
		slli r15, r15, 1					/* shift left, but need to take care if it's the reaches the leftmost position */
		stw r15, (r14)						/* write into memory LED_PATTERN */
		ldw r3, PATTERN_FRONT_END(r0)
		bne r15, r3, END_TIMER_ISR
		mov r2, r0							/* if reach the left most, reverse direction to right by setting r2 to 0 */
		stw r2, (r13)						/* write into memory LED_DIRECTION */
			
	SHIFT_RIGHT:
		srli r15, r15, 1					/* shift right, but need to take care if it's the reaches the rightmost position */
		stw r15, (r14)						/* write into memory LED_PATTERN */
		ldw r4, PATTERN_BACK_END(r0)
		bne r15, r4, END_TIMER_ISR
		addi r2, r0, 1						/* if reach the right most, reverse direction to right by setting r2 to 1 */
		stw r2, (r13)						/* write into memory LED_DIRECTION */
			
END_TIMER_ISR:
	/* restore from stack */
	ldw r10, 0(sp)
	ldw r15, 4(sp)
	ldw r5, 8(sp)
	ldw r2, 12(sp)
	ldw r3, 16(sp)
	ldw r4, 20(sp)
	ldw r13, 24(sp)
	ldw r14, 28(sp)
	addi sp, sp, 32				
	
	ret
	
	.end