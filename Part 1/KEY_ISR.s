	.extern PATTERN_BACK_END
	.extern PATTERN_FRONT_END
	.extern DELAY_VALUE
	.extern LED_DIRECTION
	.extern LED_PATTERN
	.extern LED_RUNNING
	.global KEY_ISR
	
KEY_ISR:
	/* store used registers */
	subi sp, sp, 16
	stw r10, 0(sp)
	stw r8, 4(sp)
	stw r5, 8(sp)
	stw r11, 12(sp)
	
	movia r11, LED_RUNNING
	
	movia r10, 0x10000050					/* KEY base address */
	ldwio r8, 12(r10)						/* read from edgecapture register of KEY that is 12 words higher */
	stwio r0, 12(r10)						/* write to clear interrupt bit */
	
	andi r8, r8, 0b1000						/* mask to read for KEY3 only */
	beq r8, r0, END_KEY_ISR		
	ldw r5, LED_RUNNING(r0)					/* read if LED is currently running */
	
	beq r5, r0, ENABLE_RUNNING

DISABLE_RUNNING:							/* if the RUNNING is currently ENABLED, disable it */
	mov r5, r0
	stw r5,(r11)
	br END_KEY_ISR
	
ENABLE_RUNNING:								/* if the RUNNING is currently DISABLED, enable it */
	addi r5, r0, 1
	stw r5, (r11)
	br END_KEY_ISR
		
END_KEY_ISR:
	/* restore from stack */
	ldw r10, 0(sp)
	ldw r8, 4(sp)
	ldw r5, 8(sp)	
	ldw r11, 12(sp)
	addi sp, sp, 16
	ret
	
	.end
	