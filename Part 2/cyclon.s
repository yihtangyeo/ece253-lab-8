	.text
	.global _start
	
_start:
	
	movia r16, 0x10000000			/* LEDR base address */
	movia r17, 0x10000050			/* KEY base address */	
	movia sp, 0x4500
	
	/* set up delay timer */
	movia r18, 0x10002000			/* status register */
	ldw r12, DELAY_VALUE(r0)		/* get data value from memory */
	sthio r12, 8(r18)				/* store half word in low counter */
	srli r12, r12, 16				/* logical shift right by 16 */
	sthio r12, 12(r18)				/* store half word in high counter */
	movi r19, 0b0111				/* control register: set START=1 and CONT=1, also write into control register 4 words above status register at bit 1 for ITO enable */
	sthio r19, 4(r18)				/* write into control register */
	
	/* set up interrupt in KEY */
	movi r7, 0b01110				/* check interrupt in KEY 3,2,1 only */
	stwio r7, 8(r17)				/* write into interruptmask register which is 8 words above base address */
	
	movi r7, 0b011					/* IRQ1 for pushbutton, IRQ0 for timer interrupt assignment */
	wrctl ienable, r7
	movi r7, 1
	wrctl status, r7				/* enable PIE in status register */
	
	TURN_ON_THE_LIGHTS:
		ldw r15, LED_PATTERN(r0)	/* load pattern from memory*/
		stwio r15, 0(r16)			/* show pattern on LEDR */
	br TURN_ON_THE_LIGHTS			
	
	.data
.global PATTERN_BACK_END
	PATTERN_BACK_END:
	.word 0x00000001
.global PATTERN_FRONT_END
	PATTERN_FRONT_END:
	.word 0x00040000
.global DELAY_VALUE
	DELAY_VALUE:
	.word 0x200000
.global LED_DIRECTION
	LED_DIRECTION:
	.word 1
.global LED_PATTERN
	LED_PATTERN:
	.word 0x00000001
.global LED_RUNNING
	LED_RUNNING:
	.word 1
	.end