	.extern PATTERN_BACK_END
	.extern PATTERN_FRONT_END
	.extern DELAY_VALUE
	.extern LED_DIRECTION
	.extern LED_PATTERN
	.extern LED_RUNNING
	.global KEY_ISR
	
KEY_ISR:
	/* store used registers */
	subi sp, sp, 28
	stw r10, 0(sp)
	stw r8, 4(sp)
	stw r7, 8(sp)
	stw r5, 12(sp)
	stw r11, 16(sp)
	stw r12, 20(sp)
	stw r18, 24(sp)
	
	movia r11, LED_RUNNING
	
	movia r10, 0x10000050					/* KEY base address */
	ldwio r8, 12(r10)						/* read from edgecapture register of KEY that is 12 words higher */
	stwio r0, 12(r10)						/* write to clear interrupt bit */
	
	andi r7, r8, 0b1000						/* mask to read for KEY3 only */
	bne r7, r0, CHECK_KEY3
	andi r7, r8, 0b0100						/* mask to read for KEY2 only */
	bne r7, r0, CHECK_KEY2
	andi r7, r8, 0b0010						/* mask to read for KEY1 only */
	bne r7, r0, CHECK_KEY1
	
	br END_KEY_ISR							/* else if no condition is met, branch to the end */

CHECK_KEY3:
	ldw r5, LED_RUNNING(r0)					/* read if LED is currently running */	
	beq r5, r0, ENABLE_RUNNING

	DISABLE_RUNNING:							/* if the RUNNING is currently ENABLED, disable it */
		mov r5, r0
		stw r5,(r11)
		br END_KEY_ISR
		
	ENABLE_RUNNING:								/* if the RUNNING is currently DISABLED, enable it */
		addi r5, r0, 1
		stw r5, (r11)
		br END_KEY_ISR

CHECK_KEY2:
	movia r12, DELAY_VALUE
	ldw r5, (r12)
	slli r5, r5, 1
	stw r5, (r12)
	
	movia r18, 0x10002000					/* status register */
	sthio r5, 8(r18)						/* store half word in low counter */
	srli r5, r5, 16							/* lofical shift right by 16 */
	sthio r5, 12(r18)						/* store half word in high counter */
	movi r5, 0b0111							/* control register: set START=1 and CONT=1 */
	sthio r5, 4(r18)						/* write into control register */
	
	br END_KEY_ISR

CHECK_KEY1:
	movia r12, DELAY_VALUE
	ldw r5, (r12)
	srli r5, r5, 1
	stw r5, (r12)
	
	movia r18, 0x10002000					/* status register */
	sthio r5, 8(r18)						/* store half word in low counter */
	srli r5, r5, 16							/* lofical shift right by 16 */
	sthio r5, 12(r18)						/* store half word in high counter */
	movi r5, 0b0111							/* control register: set START=1 and CONT=1 */
	sthio r5, 4(r18)						/* write into control register */
	
	br END_KEY_ISR	

		
END_KEY_ISR:
	/* restore from stack */
	ldw r10, 0(sp)
	ldw r8, 4(sp)
	ldw r7, 8(sp)
	ldw r5, 12(sp)
	ldw r11, 16(sp)
	ldw r12, 20(sp)
	ldw r18, 24(sp)
	addi sp, sp, 28
	ret
	
	.end
	