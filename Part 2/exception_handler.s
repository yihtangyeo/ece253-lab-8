	.section	.reset, "ax"
	movia r2, _start
	jmp r2
	
	.section 	.exceptions, "ax"
	.global EXCEPTION_HANDLER

EXCEPTION_HANDLER:
	subi sp, sp, 16
	stw et, 0(sp)					/* store exception temporary (store it on stack) */
	rdctl et, ipending
	beq et, r0, SKIP				/* if et is 0, means interrupt is internal, skip currently executing instruction */
	subi ea, ea, 4					/* go back to execute current instruction when interrupt occurs */
	
SKIP:
	/* store items on stack */
	stw ea, 4(sp)
	stw ra, 8(sp)
	stw r22, 12(sp)

	bne et, r0, CHECK_IRQ1			/* if et is not zero, it is a valid interrupt */
	
NOT_IRQ:
	br END_ISR
	
CHECK_IRQ1:
	andi r22, et, 0b10 				/* mask bits other than at position 1 because we are checking for IRQ1 */
	beq r22, r0, CHECK_IRQ0			/* if it's not IRQ1, just skip to check timer */
	call KEY_ISR
	br END_ISR
	
CHECK_IRQ0:
	andi r22, et, 0b01 				/* mask bits other than at position 0 because we are checking for IRQ0 */
	beq r22, r0, END_ISR			/* if it's not IRQ0, just skip to the end */
	call TIMER_ISR
	br END_ISR
	
END_ISR:
	ldw et, 0(sp)
	ldw ea, 4(sp)
	ldw ra, 8(sp)
	ldw r22, 12(sp)
	addi sp, sp, 16
	
	eret
	.end
	
	